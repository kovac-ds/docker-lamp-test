FROM php:7.3-apache

COPY . /app
COPY ./.docker/apache/vhost.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /app
RUN apt-get update && apt-get upgrade -y && apt-get install -y mc iputils-ping

RUN docker-php-ext-install pdo pdo_mysql mysqli

# Install PECL extensions
RUN pecl install xdebug-2.7.0beta1
COPY ./.docker/php/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
RUN echo "\nzend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" >> /usr/local/etc/php/conf.d/xdebug.ini
#RUN docker-php-ext-enable xdebug


RUN a2enmod rewrite